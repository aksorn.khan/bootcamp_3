package com.example.retrofitapi.data

import com.google.gson.annotations.SerializedName

data class BeerModel(
    @SerializedName("name")
    val name: String?,

    @SerializedName("description")
    val description: String?,

    @SerializedName("abv")
    val abv: Double?,

    @SerializedName("image_url")
    val imageUrl: String?


)