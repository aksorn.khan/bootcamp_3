package com.example.retrofitapi.fragment_sample.ui.main

import com.example.retrofitapi.ui.BeerInterface
import com.example.retrofitapi.ui.BeerPresenter

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.example.retrofitapi.R
import com.example.retrofitapi.data.BeerModel
import com.example.retrofitapi.service.BeerManager
import com.example.retrofitapi.ui.BeerActivity
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_beer.*
import java.time.Instant

class FregmentBeerActivity: Fragment(), BeerInterface {

    companion object {
        fun newInstance(): FregmentBeerActivity = FregmentBeerActivity()
    }

    private val presenter = BeerPresenter(this)

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.activity_beer, container,false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter.getBeerApi()
        setView()

    }

    private fun setView() {
        btnRefresh.setOnClickListener{
            presenter.getBeerApi()
        }
    }

    override fun setBeer(beerItem: BeerModel) {
        Picasso.get().load(beerItem.imageUrl).into(this.imageBeer)
        nameBeer.text = "${beerItem.name}"
        descriptionBeer.text = "${beerItem.description}"
        abv.text = "(${beerItem.abv}%)"
    }


}