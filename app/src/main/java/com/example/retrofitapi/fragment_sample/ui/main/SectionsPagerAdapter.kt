package com.example.retrofitapi.fragment_sample.ui.main

import android.content.Context
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.example.retrofitapi.R
import com.example.retrofitapi.fragment_sample.data.FragmentModel


//private val TAB_TITLES = arrayOf(
//    R.string.tab_text_1,
//    R.string.tab_text_2
//)

class SectionsPagerAdapter(private val context: Context,
                           private val fmList: List<FragmentModel> ,
                           fm: FragmentManager) : FragmentPagerAdapter(fm) {

    override fun getItem(position: Int): Fragment {
        return fmList[position].fragment
    }

    override fun getPageTitle(position: Int): CharSequence? {
//        return context.resources.getString(TAB_TITLES[position])
//        return if (position == 0) {
//            "First"
//        } else {
//            "Other"
//        }
        return fmList[position].tabName

    }

    override fun getCount(): Int {
        return fmList.count()
    }
}