package com.example.retrofitapi.fragment_sample

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.retrofitapi.R
import com.example.retrofitapi.fragment_sample.data.FragmentModel
import com.example.retrofitapi.fragment_sample.ui.main.FregmentBeerActivity
import com.example.retrofitapi.fragment_sample.ui.main.PlaceholderFragment
import com.example.retrofitapi.fragment_sample.ui.main.ProfileFragment
import com.example.retrofitapi.fragment_sample.ui.main.SectionsPagerAdapter
import com.example.retrofitapi.ui.BeerActivity
import kotlinx.android.synthetic.main.activity_home.*


class HomeActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
        setView()
    }

    fun setView(){
        val tabList = listOf<FragmentModel>(
            FragmentModel("Home",PlaceholderFragment.newInstance(123)),
            FragmentModel("Info",PlaceholderFragment.newInstance(456)),
            FragmentModel("Profile", ProfileFragment.newInstance()),
            FragmentModel("Beer" ,FregmentBeerActivity.newInstance())

//            ,
//            PlaceholderFragment.newInstance(456)
        )


        val sectionsPagerAdapter = SectionsPagerAdapter(this, tabList ,supportFragmentManager)
//        val viewPager: ViewPager = findViewById(R.id.view_pager)
        viewPager.adapter = sectionsPagerAdapter
//        val tabs: TabLayout = findViewById(R.id.tabs)
        tabs.setupWithViewPager(viewPager)
//        val fab: FloatingActionButton = findViewById(R.id.fab)
//
//        fab.setOnClickListener { view ->
//            Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                .setAction("Action", null).show()
//        }
    }


}