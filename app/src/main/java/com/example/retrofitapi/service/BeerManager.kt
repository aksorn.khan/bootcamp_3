package com.example.retrofitapi.service

import com.example.retrofitapi.service.BeerApiService
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class BeerManager {
    companion object {
        const val BASE_BEER_API = "https://api.punkapi.com/"
    }

    fun createService(): BeerApiService =
        Retrofit.Builder()
            .baseUrl(BASE_BEER_API)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
            .run { create(BeerApiService::class.java) }





}