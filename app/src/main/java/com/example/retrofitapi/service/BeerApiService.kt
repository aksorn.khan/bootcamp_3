package com.example.retrofitapi.service

import com.example.retrofitapi.data.BeerModel
import retrofit2.Call
import retrofit2.http.GET

interface BeerApiService {

    @GET("v2/beers/random")
    fun getRandomBeer(): Call<List<BeerModel>>
}