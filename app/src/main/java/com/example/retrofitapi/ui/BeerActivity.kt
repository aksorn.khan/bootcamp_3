package com.example.retrofitapi.ui

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.retrofitapi.R
import com.example.retrofitapi.data.BeerModel
import com.example.retrofitapi.service.BeerManager
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_beer.*
import java.time.Instant

class BeerActivity: AppCompatActivity(), BeerInterface {

    private val presenter = BeerPresenter(this)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_beer)
        presenter.getBeerApi()
        setView()

    }
    private fun setView() {
        btnRefresh.setOnClickListener{
            presenter.getBeerApi()
        }
    }

    override fun setBeer(beerItem: BeerModel) {
        nameBeer.text = "${beerItem.name}"
        descriptionBeer.text = "${beerItem.description}"
        abv.text = "(${beerItem.abv}%)"



        Picasso.get().load(beerItem.imageUrl).into(this.imageBeer)

//        Picasso.get()
//            .load(beerItem.imageUrl)
//            .resize(600, 1000)
////            .centerCrop()
//            .into(imageBeer)


    }


}