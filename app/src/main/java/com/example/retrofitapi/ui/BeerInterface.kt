package com.example.retrofitapi.ui

import com.example.retrofitapi.data.BeerModel

interface BeerInterface {
    fun setBeer(beerList: BeerModel)
}