package com.example.retrofitapi.ui

import android.widget.TextView
import com.example.retrofitapi.data.BeerModel
import com.example.retrofitapi.service.BeerManager
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class BeerPresenter(val view: BeerInterface) {

    fun getBeerApi(){
        BeerManager().createService().getRandomBeer().enqueue(object : Callback<List<BeerModel>> {
            override fun onFailure(call: Call<List<BeerModel>>, t: Throwable) {
                println("FAILED !")
            }

            override fun onResponse(call: Call<List<BeerModel>>, response: Response<List<BeerModel>>) {
                response.body()?.apply {
                    if (this.isNotEmpty()) {
                        view.setBeer(this[0])
                    }
                }
            }

        })
    }

}